# Modelling Cryptographic Distinguishers Using Machine Learning - Code #

### Content of the Repo ###
* `algorithms`: all the NIST DRBGs
* `experiments_cvs/plots.R`: code for plotting the obtained CSV (as in the paper)
* `ml`: the machine learning framework used
* `targetDatasets` and `trainingDatasets`: temporary folder for target and training dataset
* `main.py`: main code to execute the experiment and contains the experiment parameters


### Quick Usage ###
* Modify the experimental parameters in `main.py`
* Run `main.py` 
